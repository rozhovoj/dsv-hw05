#!/usr/bin/env python3
import pika
import random

# no credentials for localhost necessary
credentials = pika.PlainCredentials('root', 'dsv')
parameters = pika.ConnectionParameters('160.217.213.154', 5672, '/', credentials)
connection = pika.BlockingConnection(parameters)
channel = connection.channel()
channel.queue_declare(queue='number')

#exchange not specified, default is direct
x = random.randint(0, 1000)
channel.basic_publish(exchange='', routing_key='hello', body=str(x))
print(" [x] Sent ", x)
connection.close()
