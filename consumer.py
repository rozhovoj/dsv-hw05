#!/usr/bin/env python3
import pika

credentials = pika.PlainCredentials('root', 'dsv')
parameters = pika.ConnectionParameters('160.217.213.154', 5672, '/', credentials)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.queue_declare(queue='prime')


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)


channel.basic_consume(queue='prime', on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()




