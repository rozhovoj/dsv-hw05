#!/usr/bin/env python3
import pika

credentials = pika.PlainCredentials('root', 'dsv')
parameters = pika.ConnectionParameters('160.217.213.154', 5672, '/', credentials)

connection = pika.BlockingConnection(parameters)
channel = connection.channel()

channel.queue_declare(queue='number')
channel.queue_declare(queue='prime')


def is_prime(n):
    for i in range(2, n):
        if (n % i) == 0:
            return False
    return True


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)
    if is_prime(int(body)):
        channel.basic_publish(exchange='', routing_key='prime', body=str(body))


channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
